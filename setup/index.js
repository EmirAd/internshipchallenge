const layerContainer = document.querySelector('.layout-container');
const cardContainer = document.createElement('div');
cardContainer.classList.add('cardContainer');
layerContainer.append(cardContainer);

const loadMoreBtn = document.createElement('button');
loadMoreBtn.classList.add('load-more');
loadMoreBtn.innerText = 'Load More';

const themeSwitch = document.querySelector('input[type="checkbox"]');

fetch('./data.json')
  .then((res) => res.json())
  .then((data) => {
    data.forEach((el) => {
      const fullDate = new Date(el.date);
      const day = new Date(el.date).getDay();
      const month = fullDate.toLocaleString('default', { month: 'short' });
      const year = new Date(el.date).getFullYear();

      const card = document.createElement('div');
      const popupCard = document.createElement('div');

      card.classList.add('card');
      popupCard.classList.add('popup');

      card.innerHTML = `
    <div class="wrapper">
    <div class="cardHeader">
    <div class="profile">
    <img src=${el.profile_image} alt="profile" />
    <p>${el.name}</p>
    </div>
    <div>
    <p class="date">${day + ' ' + month + ' ' + year} </p>
    </div>
    </div>
        <div class="image">
        <img src=${el.image} alt="img"/>
        </div>
        <div class="cardFooter">
        <img src=${
          el.source_type === 'facebook'
            ? './icons/facebook.svg'
            : './icons/instagram-logo.svg'
        } alt="social" class="type"/>
         
            <i class="fa-regular fa-heart"></i>
      
        <p class="likes">${+el.likes}</p>
        <p>${el.caption}</p>
        
        </div>
        </div>  
        `;

      popupCard.innerHTML = `
        <div class="overlay">
        <div class="close">X</div>
          <div class="imagePop">
            <img src=${el.image}/>
          </div>
        </div>
        
        `;

      const closeBtn = popupCard.querySelector('.close');
      closeBtn.addEventListener('click', (e) => {
        popupCard.style.display = 'none';
      });

      cardContainer.append(card, popupCard, loadMoreBtn);
      themeSwitch.addEventListener('click', (e) => {
        if (e.target.checked) {
          document.body.classList.add('darkTheme');
          card.classList.add('darkTheme');
        } else {
          document.body.classList.remove('darkTheme');
          card.classList.remove('darkTheme');
        }
      });
      const image = card.querySelector('.image');
      image.addEventListener('click', (e) => {
        popupCard.style.display = 'block';
      });
    });
  });

document.addEventListener('click', (e) => {
  if (e.target.classList.contains('fa-regular')) {
    e.target.nextElementSibling.innerHTML =
      +e.target.nextElementSibling.innerHTML + 1;
    e.target.classList.remove('fa-regular');
    e.target.classList.add('fa-solid');
  } else if (e.target.classList.contains('fa-solid')) {
    e.target.nextElementSibling.innerHTML =
      +e.target.nextElementSibling.innerHTML - 1;
    e.target.classList.remove('fa-solid');
    e.target.classList.add('fa-regular');
  }
});
window.addEventListener('click', () => {
  const card = document.querySelectorAll('.card');

  let currentItem = 4;
  loadMoreBtn.addEventListener('click', function (e) {
    for (let i = currentItem; i < currentItem + 4; i++) {
      if (card[i]) {
        card[i].style.display = 'flex';
      }
    }
    currentItem += 4;
    if (currentItem >= card.length) {
      e.target.style.display = 'none';
    }
  });
});
